#ifndef DNest4_Template_MyModel
#define DNest4_Template_MyModel

#include "DNest4/code/DNest4.h"
#include <ostream>

class MyModel
{
	private:
        // Parameters
        double mu1, mu2;

        // Latent variables for making a datasets from the parameters
        std::vector<double> n1, n2;

        // Two datasets
        std::vector<double> x1, x2;

        // Log likelihoods
        double logl1, logl2;
        void calculate_log_likelihoods();

	public:
		// Constructor only gives size of params
		MyModel();

		// Generate the point from the prior
		void from_prior(DNest4::RNG& rng);

		// Metropolis-Hastings proposals
		double perturb(DNest4::RNG& rng);

		// Likelihood function
		double log_likelihood() const;

		// Print to stream
		void print(std::ostream& out) const;

		// Return string with column information
		std::string description() const;

        // A getter
        double get_logl1() const
        { return logl1; }
};

#endif

