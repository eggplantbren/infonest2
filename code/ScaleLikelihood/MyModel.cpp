#include "MyModel.h"
#include "DNest4/code/DNest4.h"

using namespace std;
using namespace DNest4;

MyModel::MyModel()
:n1(100), n2(100), x1(100), x2(100)
{

}

void MyModel::from_prior(RNG& rng)
{
    mu1 = 1000*rng.randn();
    mu2 = 1000*rng.randn();

    for(size_t i=0; i<n1.size(); ++i)
    {
        n1[i] = rng.randn();
        n2[i] = rng.randn();
    }

    calculate_log_likelihoods();
}

void MyModel::calculate_log_likelihoods()
{
    // Assemble simulated data
    for(size_t i=0; i<n1.size(); ++i)
    {
        x1[i] = mu1 + n1[i];
        x2[i] = mu2 + n2[i];
    }

    logl1 = 0.0;
    logl2 = 0.0;

    static constexpr double C = -0.5*log(2*M_PI);

    for(size_t i=0; i<x1.size(); ++i)
    {
        logl1 += C - 0.5*pow(x1[i] - mu1, 2);
        logl2 += C - 0.5*pow(x2[i] - mu1, 2);
    }
}

double MyModel::perturb(RNG& rng)
{
	double logH = 0.0;

    int which = rng.rand_int(4);
    if(which == 0)
    {
        logH -= -0.5*pow(mu1/1E3, 2);
        mu1 += 1E3*rng.randh();
        logH += -0.5*pow(mu1/1E3, 2);
    }
    if(which == 1)
    {
        logH -= -0.5*pow(mu2/1E3, 2);
        mu2 += 1E3*rng.randh();
        logH += -0.5*pow(mu2/1E3, 2);
    }
    if(which == 2)
    {
        int i = rng.rand_int(n1.size());
        logH -= -0.5*pow(n1[i], 2);
        n1[i] += rng.randh();
        logH += -0.5*pow(n1[i], 2);
    }
    if(which == 3)
    {
        int i = rng.rand_int(n2.size());
        logH -= -0.5*pow(n2[i], 2);
        n2[i] += rng.randh();
        logH += -0.5*pow(n2[i], 2);
    }

    calculate_log_likelihoods();

	return logH;
}

double MyModel::log_likelihood() const
{
	return logl2;
}

void MyModel::print(std::ostream& out) const
{
    out<<logl1<<' '<<logl2;
}

string MyModel::description() const
{
	return string("logl1");
}

