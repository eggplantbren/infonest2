#include <iostream>
#include <fstream>
#include "DNest4/code/DNest4.h"
#include "MyModel.h"

using namespace DNest4;


int main(int argc, char** argv)
{
    // Generate from p1(theta, D) = p(theta)p(D|theta)
    DNest4::RNG rng;
    rng.set_seed(time(0));
    MyModel m;
    std::fstream fout("logl_p1.txt", std::ios::out);
    for(int i=0; i<1000000; ++i)
    {
        m.from_prior(rng);
        fout<<std::setprecision(12)<<m.get_logl1()<<std::endl;
        std::cout<<(i+1)<<std::endl;
    }
    fout.close();

    DNest4::start<MyModel>(argc, argv);
	return 0;
}

