import dnest4.classic as dn4
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rng

logx_samples = dn4.postprocess(verbose=False)[-1]
logl_samples = dn4.my_loadtxt("sample_info.txt")[:,1]
ii = np.argsort(logl_samples)
logx_samples = logx_samples[ii]
logl_samples = logl_samples[ii]
index_samples = np.arange(0, len(logx_samples))

def find_indices(logl):
    result = np.empty(len(logl))
    for i in range(0, len(logl)):
        index = 0
        while logl[i] > logl_samples[index]:
            index += 1
        result[i] = index + rng.rand()
        if (i+1)%1000 == 0:
            print("Done {k}".format(k=i+1))
    return result

logl = dn4.my_loadtxt("logl_p1.txt").flatten()
indices = find_indices(logl)

plt.hist(indices, 500, color="g", alpha=0.1, normed=True)
plt.show()

