from pylab import *

x = linspace(-10, 10, 1001)
dA = (x[1] - x[0])**2
[x, y] = meshgrid(x, x)
y = y[::-1, :]

p0 = exp(-0.5*x**2 - 0.5*(y**2/2))
p1 = exp(-0.5*x**2 - 0.5*(y - x)**2)
p0 /= p0.sum()*dA
p1 /= p1.sum()*dA

py = exp(-0.5*y[:,0]**2/2)
py /= abs(trapz(py, x=y[:,0]))

subplot(1,2,1)
imshow(p0)
subplot(1,2,2)
imshow(p1)
show()

I = dA*sum(p1*log(p1/p0))
Hy = -trapz(py*log(py), x=y[::-1,0])

print("Mutual information = {I}".format(I=I))
print("Entropy of p(y) = {Hy}".format(Hy=Hy))

p2 = exp(-0.5*x**2)
p2 /= p2.sum()*dA
epsilon = 0.1
p3 = p2*(abs(x - y) < epsilon)
p3 /= p3.sum()*dA

p3x = p3.sum(axis=0)
p3x /= trapz(p3x, x=x[0, :])
print(trapz(p3x*x[0, :]**2, x=x[0, :]))

plot(x[0, :], p3x)
show()

