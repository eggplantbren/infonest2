#include "Data.h"
#include <fstream>
#include <iostream>

using namespace std;

Data Data::instance;

Data::Data()
{

}

void Data::load(const char* filename0, const char* filename1)
{
	fstream fin;

    fin.open(filename0, ios::in);
	if(!fin)
	{
		cerr<<"# Error. Couldn't open file "<<filename0<<"."<<endl;
		return;
	}

	// Empty the vector
	x0.clear();

	double temp1;
	while(fin>>temp1)
		x0.push_back(temp1);

	cout<<"# Loaded "<<x0.size()<<" data points from file "
			<<filename0<<"."<<endl;
	fin.close();

    fin.open(filename1, ios::in);
	if(!fin)
	{
		cerr<<"# Error. Couldn't open file "<<filename1<<"."<<endl;
		return;
	}

	// Empty the vector
	x1.clear();

	while(fin>>temp1)
		x1.push_back(temp1);

	cout<<"# Loaded "<<x1.size()<<" data points from file "
			<<filename1<<"."<<endl;
	fin.close();

    x0_min = *min_element(x0.begin(), x0.end());
    x0_max = *max_element(x0.begin(), x0.end());
    x1_min = *min_element(x1.begin(), x1.end());
    x1_max = *max_element(x1.begin(), x1.end());

    x0_range = x0_max - x0_min;
    x1_range = x1_max - x1_min;
}

