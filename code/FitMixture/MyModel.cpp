#include "MyModel.h"
#include "DNest4/code/DNest4.h"
#include "Data.h"
#include <limits>

using namespace std;
using namespace DNest4;

MyModel::MyModel()
:gaussians0(3, 100, false,
                MyConditionalPrior(Data::get_instance().get_x0_min(),
                                   Data::get_instance().get_x0_max()),
                PriorType::log_uniform)
,gaussians1(3, 100, false,
                MyConditionalPrior(Data::get_instance().get_x1_min(),
                                   Data::get_instance().get_x1_max()),
                PriorType::log_uniform)
{

}

void MyModel::from_prior(RNG& rng)
{
    do
    {
        gaussians0.from_prior(rng);
    }while(gaussians0.get_components().size() == 0);

    do
    {
        gaussians1.from_prior(rng);
    }while(gaussians1.get_components().size() == 0);
}

double MyModel::perturb(RNG& rng)
{
	double logH = 0.0;

    if(rng.rand() <= 0.5)
        logH += gaussians0.perturb(rng);
    else
        logH += gaussians1.perturb(rng);

    if(gaussians0.get_components().size() == 0 ||
        gaussians1.get_components().size() == 0)
        return -std::numeric_limits<double>::max();

	return logH;
}

double MyModel::log_likelihood() const
{
	double logL = 0.0;

    // Extract parameters
    vector<double> mu, sigma, log_weight, C;
    const auto& components = gaussians0.get_components();
    for(const auto& c: components)
    {
        mu.push_back(c[0]);
        sigma.push_back(exp(c[1]));
        log_weight.push_back(c[2]);
        C.push_back(0.5*log(2*M_PI*sigma.back()*sigma.back()));
    }

    // Normalise weights
    double tot = logsumexp(log_weight);
    for(double& lw: log_weight)
        lw -= tot;

    // Get data
    const vector<double>& data = Data::get_instance().get_x0();

    // Loop over data
    for(double x: data)
    {
        // Log-likelihood for a single data point
        double _logL = -std::numeric_limits<double>::max();

        // Loop over mixture components
        for(size_t j=0; j<mu.size(); ++j)
        {
            _logL = logsumexp(_logL,
                                log_weight[j] - C[j]
                                -0.5*pow((x - mu[j])/sigma[j], 2));
        }

        logL += _logL;
    }


    const auto& components1 = gaussians1.get_components();
    mu.clear(); sigma.clear(); log_weight.clear(); C.clear();
    for(const auto& c: components1)
    {
        mu.push_back(c[0]);
        sigma.push_back(exp(c[1]));
        log_weight.push_back(c[2]);
        C.push_back(0.5*log(2*M_PI*sigma.back()*sigma.back()));
    }

    // Normalise weights
    tot = logsumexp(log_weight);
    for(double& lw: log_weight)
        lw -= tot;

    // Get data
    const vector<double>& data1 = Data::get_instance().get_x1();

    // Loop over data
    for(double x: data1)
    {
        // Log-likelihood for a single data point
        double _logL = -std::numeric_limits<double>::max();

        // Loop over mixture components
        for(size_t j=0; j<mu.size(); ++j)
        {
            _logL = logsumexp(_logL,
                                log_weight[j] - C[j]
                                -0.5*pow((x - mu[j])/sigma[j], 2));
        }

        logL += _logL;
    }

	return logL;
}

void MyModel::print(std::ostream& out) const
{
    gaussians0.print(out);
    gaussians1.print(out);
}

string MyModel::description() const
{
	return string("");
}

