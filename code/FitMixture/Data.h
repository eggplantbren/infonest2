#ifndef DNest4_Data
#define DNest4_Data

#include <vector>
#include <algorithm>

class Data
{
	private:
		std::vector<double> x0;
        std::vector<double> x1;

        // Summaries
        double x0_min, x0_max, x1_min, x1_max, x0_range, x1_range;

	public:
		Data();
		void load(const char* filename0, const char* filename1);

		// Getters
		const std::vector<double>& get_x0() const { return x0; }
        const std::vector<double>& get_x1() const { return x1; }

        double get_x0_min() const { return x0_min; }
        double get_x0_max() const { return x0_max; }
        double get_x0_range() const { return x0_range; }

        double get_x1_min() const { return x1_min; }
        double get_x1_max() const { return x1_max; }
        double get_x1_range() const { return x1_range; }

	// Singleton
	private:
		static Data instance;
	public:
		static Data& get_instance() { return instance; }
};

#endif

