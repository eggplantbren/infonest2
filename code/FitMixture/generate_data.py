import numpy as np
import numpy.random as rng
import matplotlib.pyplot as plt

# Seed RNG
rng.rand(123)

# Number of samples from each distribution
N0, N1 = 1000, 1000

# Generate from p0
x0 = -10 + 20*rng.rand(N0)

# Generate from p1
x1 = rng.randn(N1)

# The actual value of KL(p1 || p0) is 1.129 nats.
np.savetxt("data0.txt", x0)
np.savetxt("data1.txt", x1)

# Histogram the samples
bins = np.linspace(-20.0, 20.0, 200)

plt.hist(x0, bins=bins, color="k", alpha=0.1, normed=True)
plt.hist(x1, bins=bins, color="g", alpha=0.1, normed=True)
plt.show()

