from pylab import *
import dnest4.classic as dn4

rc("font", size=18, family="serif", serif="Computer Sans")
rc("text", usetex=True)

data0, data1 = loadtxt('data0.txt'), loadtxt("data1.txt")
posterior_sample = atleast_2d(dn4.my_loadtxt('posterior_sample.txt'))

left = min([min(data0), min(data1)])
right = max([max(data0), max(data1)])
x = linspace(left - 0.5*(right-left), right + 0.5*(right-left), 100001)

def mixture(x, params):
    N = int(params[7])
    centers = params[8:108][0:N]
    widths = exp(params[108:208][0:N])
    weights = exp(params[208:308][0:N])
    weights /= weights.sum()

    y = zeros(x.shape)
    for i in range(0, N):
        y += weights[i]/widths[i]/sqrt(2.*pi)*exp(-0.5*(x - centers[i])**2/widths[i]**2)

    return y

hold(False)
bins = linspace(left, right, 201)
hist(data0, bins, alpha=0.2, color="k", normed=True)
hold(True)
hist(data1, bins, alpha=0.2, color="g", normed=True)
y0_tot = zeros(len(x))
y1_tot = zeros(len(x))

KL = []
for i in range(0, posterior_sample.shape[0]):
    y0 = mixture(x, posterior_sample[i, 0:posterior_sample.shape[1]//2])
    y1 = mixture(x, posterior_sample[i, posterior_sample.shape[1]//2:])
    y0_tot += y0
    y1_tot += y1
    KL.append(trapz(y1*log(y1/(y0 + 1E-300) + 1E-300), x=x))

hold(True)
plot(x, y0_tot/posterior_sample.shape[0], 'k', linewidth=2)
plot(x, y1_tot/posterior_sample.shape[0], 'g', linewidth=2)

xlabel("$x$", fontsize=20)
ylabel("Density")
show()

hist(KL, 100, color="k", alpha=0.1)
axvline(1.5768, color="r")
xlabel("KL$(p_1 || p_0)$")
ylabel("Posterior Samples")
show()


